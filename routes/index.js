const express = require('express');
const router = express.Router();

module.exports = () => {
    router.use('/admin', require('./admin'));
    router.use('/api', require('./api'));

    return router;
};