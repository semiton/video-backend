const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 100),
        page = Number(req.query.page || 1),
        offset = (page - 1) * limit,
        channelId = req.query.channelId || null,
        categoryId = req.query.categoryId || null;

    let where = {};
    if (channelId) {
        where['channelId'] = channelId;
    }
    if (categoryId) {
        where['categoryId'] = categoryId;
    }

    models.Video
        .findAndCountAll({
            where: where,
            offset: offset,
            limit: limit,
            include: [{
                model: models.Channel,
                as: 'channel',
                include: [{
                    model: models.Author,
                    as: 'author',
                }]
            }, {
                model: models.Category,
                as: 'category'
            }, 'tags']
        })
        .then(result => {
            return res.json({
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });

        })
        .catch(next);
});

router.get('/:id(\\d+)', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    return models.Video
        .findOne({
            where: {
                id: id
            },
            include: [{
                model: models.Channel,
                as: 'channel',
                include: [{
                    model: models.Author,
                    as: 'author',
                }]
            }, {
                model: models.Category,
                as: 'category'
            }, 'tags']
        })
        .then(author => {
            if (author === null) {
                req.status = 404;
                return next();
            }

            return res.json(author);
        })
        .catch(next);
});

module.exports = router;
