const config = require('config');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.all('/token', (req, res, next) => {
    let username = String((req.body['username'] || req.query['username']) || '').trim(),
        password = String((req.body['password'] || req.query['password']) || '').trim(),
        Admin = req.app.get('models').Admin,
        sessionSecret = config.get('session.secret'),
        sessionAge = config.get('session.cookie.maxAge');

    if (username.length === 0 || password.length === 0) {
        return next({
            status: 400,
            messsage: 'username or password is empty'
        });
    }

    return Admin
        .findOne({
            where: {
                username: username,
                password: Admin.hashPassword(password)
            }
        })
        .then(user => {
            if (!user) {
                return next({
                    status: 400,
                    messsage: 'Incorrect username or password.'
                });
            }

            return jwt.sign(user.dataValues, sessionSecret, { expiresIn: sessionAge }, (error, token) => {
                if (error) {
                    return next({
                        status: 400,
                        messsage: error.toString(),
                    });
                }

                return res.json({
                    token: token,
                    expires: Number(Date.now() + sessionAge)
                });
            });
        })
        .catch(error => {
            return next({
                status: 500,
                messsage: error.toString(),
            });
        });
});

module.exports = router;