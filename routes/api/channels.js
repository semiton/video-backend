const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 100),
        page = Number(req.query.page || 1),
        offset = (page - 1) * limit,
        authorId = req.query.authorId || null;

    let where = {};
    if (authorId) {
        where = {
            authorId: authorId,
        };
    }

    models.Channel
        .findAndCountAll({
            where: where,
            offset: offset,
            limit: limit,
            include: [{
                model: models.Author,
                as: 'author',
            }]
        })
        .then(result => {
            return res.json({
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });

        })
        .catch(next);
});

router.get('/:id(\\d+)', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    return models.Channel
        .findOne({
            where: {
                id: id
            },
            include: [{
                model: models.Author,
                as: 'author',
            }]
        })
        .then(channel => {
            if (channel === null) {
                req.status = 404;
                return next();
            }

            return res.json(channel);
        })
        .catch(next);
});

module.exports = router;
