const express = require('express');
const router = express.Router();
const authorize = require('../../lib/authorize');

router.use('/auth', require('./auth'));

router.use('/authors', authorize.api, require('./authors'));
router.use('/channels', authorize.api, require('./channels'));
router.use('/videos', authorize.api, require('./videos'));

module.exports = router;
