const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 100),
        page = Number(req.query.page || 1),
        offset = (page - 1) * limit;

    return models.Author
        .findAndCountAll({
            offset: offset,
            limit: limit,
        })
        .then(result => {
            return res.json({
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });

        })
        .catch(next);
});

router.get('/:id(\\d+)', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    return models.Author
        .findOne({
            where: {
                id: id
            }
        })
        .then(author => {
            if (author === null) {
                req.status = 404;
                return next();
            }

            return res.json(author);
        })
        .catch(next);
});

module.exports = router;
