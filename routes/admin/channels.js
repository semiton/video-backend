const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 10),
        page = req.query.page || 1,
        offset = (page - 1) * limit;

    models.Channel
        .findAndCountAll({
            offset: offset,
            limit: limit,
            include: [{
                model: models.Author,
                as: 'author'
            }]
        })
        .then(result => {
            res.render('channels/index', {
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });
        })
        .catch(next);
});

router.get('/search', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 20),
        offset = 0,
        query = req.query.q;

    if (query.length === 0) {
        res.status = 400;
        return next();
    }

    models.Channel
        .findAll({
            offset: offset,
            limit: limit,
            where: {
                title: {
                    $like: query +'%'
                },
            }
        })
        .then(result => {
            let json = [];

            result.forEach((row) => {
                json.push({
                    name: row.title,
                    value: row.id,
                });
            });

            res.json({
                items: json
            });
        })
        .catch(next);
});

router.get('/add', (req, res, next) => {
    let status = req.param('status') || '';
    let title = req.param('title') || '';
    let description = req.param('description') || '';

    res.render('channels/add', {
        status: status,
        title: title,
        description: description,
    });
});

router.post('/add', (req, res, next) => {
    let models = req.app.get('models'),
        status = req.param('status') || '',
        title = req.param('title') || '',
        authorId = req.param('authorId') || '',
        description = req.param('description') || '';

    models.Channel
        .build({
            status: status,
            title: title,
            description: description,
            authorId: authorId,
        })
        .save()
        .then(() => {
            req.flash('success', 'Channel has added successfully!');
            return res.redirect('/admin/channels/add');
        })
        .catch(error => {
            req.flash('error', error.toString());
            return res.redirect('/admin/channels/add');
        });
});

router.get('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Channel
        .findOne({
            where: {
                id: id
            },
            include: [{
                model: models.Author,
                as: 'author'
            }]
        })
        .then(channel => {
            if (channel === null) {
                req.status = 404;
                return next();
            }

            res.render('channels/edit', {
                channel: channel.dataValues,
            });
        })
        .catch(next);
});

router.post('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id'),
        status = req.param('status') || '',
        title = req.param('title') || '',
        authorId = req.param('authorId') || '',
        description = req.param('description') || '';


    models.Channel
        .findById(id)
        .then(channel => {
            if (channel === null) {
                req.status = 404;
                return next();
            }

            tags = tags.trim().split(',');
            channel.setTags(tags);

            return models.Channel
                .update({
                    status: status,
                    title: title,
                    description: description,
                    authorId: authorId,
                }, {
                    where: {
                        id: channel.get('id')
                    }
                })
                .then(() => {
                    req.flash('success', 'Channel has edited successfully!');
                    return res.redirect(req.originalUrl);
                })
                .catch(error => {
                    req.flash('error', error.toString());
                    return res.redirect(req.originalUrl);
                });
        })
        .catch(next);
});

router.get('/:id(\\d+)/remove', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Channel
        .destroy({
            where: {
                id: id
            }
        })
        .then((removed) => {
            if (removed) {
                req.flash('success', 'Channel has removed successfully!');
            }
            return res.redirect('/admin/channels');
        })
        .catch(error => {
            req.flash('error', error.toString());
            return res.redirect('/admin/channels');
        });
});

module.exports = router;
