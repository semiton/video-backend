const express = require('express');
const router = express.Router();
const authorize = require('../../lib/authorize');

router.use('/login', require('./login'));
router.use('/logout', require('./logout'));

router.use('/authors', authorize.admin, require('./authors'));
router.use('/categories', authorize.admin, require('./categories'));
router.use('/channels', authorize.admin, require('./channels'));
router.use('/videos', authorize.admin, require('./videos'));
router.use('/tags', authorize.admin, require('./tags'));

router.use('/', authorize.admin, require('./main'));

module.exports = router;