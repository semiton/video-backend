const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    req.session.destroy();

    return res.redirect('/admin/login');
});

module.exports = router;