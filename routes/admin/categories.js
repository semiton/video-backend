const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 10),
        page = req.query.page || 1,
        offset = (page - 1) * limit;

    models.Category
        .findAndCountAll({
            offset: offset,
            limit: limit
        })
        .then(result => {
            res.render('categories/index', {
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });
        })
        .catch(next);
});

router.get('/search', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 20),
        offset = 0,
        query = req.query.q;

    if (query.length === 0) {
        res.status = 400;
        return next();
    }

    models.Category
        .findAll({
            offset: offset,
            limit: limit,
            where: {
                name: {
                    $like: query +'%'
                },
            }
        })
        .then(result => {
            let json = [];

            result.forEach((row) => {
                json.push({
                    name: row.name,
                    value: row.id,
                });
            });

            res.json({
                items: json
            });
        })
        .catch(next);
});

router.get('/add', (req, res, next) => {
    let name = req.param('name') || '';

    res.render('categories/add', {
        name: name,
    });
});

router.post('/add', (req, res, next) => {
    let models = req.app.get('models'),
        name = req.param('name') || '';

    if (name.length === 0) {
        req.flash('error', 'Name is empty');
        return res.redirect('/admin/categories/add');
    }

    models.Category
        .build({
            name: name
        })
        .save()
        .then(() => {
            req.flash('success', 'Category has added successfully!');
            return res.redirect('/admin/categories/add');
        })
        .catch(error => {
            req.flash('error', error);
            return res.redirect('/admin/categories/add');
        });
});

router.get('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Category
        .findById(id)
        .then(category => {
            if (category === null) {
                req.status = 404;
                return next();
            }

            res.render('categories/edit', {
                category: category.dataValues,
            });
        })
        .catch(next);
});

router.post('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id'),
        name = req.param('name');

    models.Category
        .findById(id)
        .then(category => {
            if (category === null) {
                req.status = 404;
                return next();
            }
            models.Category
                .update({
                    name: name
                }, {
                    where: {
                        id: category.get('id')
                    }
                })
                .then(() => {
                    req.flash('success', 'Category has edited successfully!');
                    return res.redirect(req.originalUrl);
                })
                .catch(error => {
                    req.flash('error', error);
                    return res.redirect(req.originalUrl);
                });
        })
        .catch(next);
});

router.get('/:id(\\d+)/remove', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Category
        .destroy({
            where: {
                id: id
            }
        })
        .then((removed) => {
            if (removed) {
                req.flash('success', 'Category has removed successfully!');
            }
            return res.redirect('/admin/categories');
        })
        .catch(error => {
            req.flash('error', error);
            return res.redirect('/admin/categories');
        });
});

module.exports = router;
