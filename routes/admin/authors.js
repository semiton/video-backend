const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 10),
        page = req.query.page || 1,
        offset = (page - 1) * limit;

    models.Author
        .findAndCountAll({
            offset: offset,
            limit: limit
        })
        .then(result => {
            res.render('authors/index', {
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });
        })
        .catch(next);
});

router.get('/search', (req, res, next) => {
    let models = req.app.get('models'),
        limit = Number(req.query.limit || 20),
        offset = 0,
        query = req.query.q;

    if (query.length === 0) {
        res.status = 400;
        return next();
    }

    models.Author
        .findAll({
            offset: offset,
            limit: limit,
            where: {
                name: {
                    $like: query +'%'
                },
            }
        })
        .then(result => {
            let json = [];

            result.forEach((row) => {
                json.push({
                    name: row.name,
                    value: row.id,
                });
            });

            res.json({
                items: json
            });
        })
        .catch(next);
});

router.get('/add', (req, res, next) => {
    let name = req.param('name') || '';

    res.render('authors/add', {
        name: name,
    });
});

router.post('/add', (req, res, next) => {
    let models = req.app.get('models'),
        name = req.param('name') || '';

    if (name.length === 0) {
        req.flash('error', 'Name is empty');
        return res.redirect('/admin/authors/add');
    }

    models.Author
        .build({
            name: name
        })
        .save()
        .then(() => {
            req.flash('success', 'Author has added successfully!');
            return res.redirect('/admin/authors/add');
        })
        .catch(error => {
            req.flash('error', error);
            return res.redirect('/admin/authors/add');
        });
});

router.get('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Author
        .findById(id)
        .then(author => {
            if (author === null) {
                req.status = 404;
                return next();
            }

            res.render('authors/edit', {
                author: author.dataValues,
            });
        })
        .catch(next);
});

router.post('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id'),
        name = req.param('name');

    models.Author
        .findById(id)
        .then(author => {
            if (author === null) {
                req.status = 404;
                return next();
            }
            models.Author
                .update({
                    name: name
                }, {
                    where: {
                        id: author.get('id')
                    }
                })
                .then(() => {
                    req.flash('success', 'Author has edited successfully!');
                    return res.redirect(req.originalUrl);
                })
                .catch(error => {
                    req.flash('error', error);
                    return res.redirect(req.originalUrl);
                });
        })
        .catch(next);
});

router.get('/:id(\\d+)/remove', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Author
        .destroy({
            where: {
                id: id
            }
        })
        .then((removed) => {
            if (removed) {
                req.flash('success', 'Author has removed successfully!');
            }
            return res.redirect('/admin/authors');
        })
        .catch(error => {
            req.flash('error', error);
            return res.redirect('/admin/authors');
        });
});

module.exports = router;
