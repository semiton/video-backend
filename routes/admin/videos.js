const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    let models = req.app.get('models'),
        limit = req.query.limit || 10,
        page = req.query.page || 1,
        offset = (page - 1) * limit;

    models.Video
        .findAndCountAll({
            offset: offset,
            limit: limit,
            include: [{
                model: models.Channel,
                as: 'channel',
                include: [{
                    model: models.Author,
                    as: 'author',
                }]
            }, {
                model: models.Category,
                as: 'category'
            }]
        })
        .then(result => {
            res.render('videos/index', {
                rows: result.rows,
                count: result.count,
                page: page,
                offset: offset,
                limit: limit
            });
        })
        .catch(next);
});

router.get('/add', (req, res, next) => {
    let status = req.param('status') || '';
    let title = req.param('title') || '';
    let description = req.param('description') || '';

    res.render('videos/add', {
        status: status,
        title: title,
        description: description,
    });
});

router.post('/add', (req, res, next) => {
    let models = req.app.get('models'),
        status = req.param('status') || '',
        title = req.param('title') || '',
        channelId = req.param('channelId') || '',
        categoryId = req.param('categoryId') || '',
        tags = req.param('tags') || '',
        description = req.param('description') || '';

    models.Video
        .build({
            status: status,
            title: title,
            description: description,
            channelId: channelId,
            categoryId: categoryId,
        })
        .save()
        .then((video) => {
            tags = tags.trim().split(',');
            video.setTags(tags);

            return video.save().then(() => {
                req.flash('success', 'Video has added successfully!');
                return res.redirect('/admin/videos/add');
            }).catch(error => {
                req.flash('error', error.toString());
                return res.redirect('/admin/videos/add');
            });
        })
        .catch(error => {
            req.flash('error', error.toString());
            return res.redirect('/admin/videos/add');
        });
});

router.get('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Video
        .findOne({
            where: {
                id: id
            },
            include: [{
                model: models.Channel,
                as: 'channel',
                include: [{
                    model: models.Author,
                    as: 'author',
                }]
            }, {
                model: models.Category,
                as: 'category'
            }, 'tags']
        })
        .then(video => {
            if (video === null) {
                req.status = 404;
                return next();
            }

            res.render('videos/edit', {
                video: video.dataValues,
            });
        })
        .catch(next);
});

router.post('/:id(\\d+)/edit', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id'),
        status = req.param('status') || '',
        title = req.param('title') || '',
        channelId = req.param('channelId') || '',
        categoryId = req.param('categoryId') || '',
        tags = req.param('tags') || '',
        description = req.param('description') || '';


    models.Video
        .findById(id)
        .then(video => {
            if (video === null) {
                req.status = 404;
                return next();
            }

            tags = tags.trim().split(',');
            video.setTags(tags);

            return models.Video
                .update({
                    status: status,
                    title: title,
                    description: description,
                    channelId: channelId,
                    categoryId: categoryId
                }, {
                    where: {
                        id: video.get('id')
                    }
                })
                .then(() => {
                    req.flash('success', 'Video has edited successfully!');
                    return res.redirect(req.originalUrl);
                })
                .catch(error => {
                    req.flash('error', error.toString());
                    return res.redirect(req.originalUrl);
                });
        })
        .catch(next);
});

router.get('/:id(\\d+)/remove', (req, res, next) => {
    let models = req.app.get('models'),
        id = req.param('id');

    models.Video
        .destroy({
            where: {
                id: id
            }
        })
        .then((removed) => {
            if (removed) {
                req.flash('success', 'Video has removed successfully!');
            }
            return res.redirect('/admin/videos');
        })
        .catch(error => {
            req.flash('error', error.toString());
            return res.redirect('/admin/videos');
        });
});

module.exports = router;
