const config = require('config');
const Sequelize = require('sequelize');

// Create instance for mysql connection
const sequelize = new Sequelize(
    config.get('database.connect.dbname'),
    config.get('database.connect.user'),
    config.get('database.connect.password'), {
        host: config.get('database.host'),
        dialect: config.get('database.dialect'),
    }
);


sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');

        if (config.get('database.sync.enable')) {
            sequelize.sync({ force: !!config.get('database.sync.force') }).then(() => {
                console.log('Connection has been established successfully.');

            }).catch(error => {
                console.error('Sync models failed:', error);
            });
        }
    })
    .catch(error => {
        console.error('Unable to connect to the database:', error);
    });


module.exports = sequelize;