const config = require('config');
const jwt = require('jsonwebtoken');

module.exports = {
    admin: (req, res, next) => {
        let redirect = () => {
            if (req.xhr) {
                return next({
                    status: 401,
                    messsage: 'Not authorized'
                });
            }

            return res.redirect('/admin/login');
        };

        if (req.session.token) {
            return jwt.verify(req.session.token, config.get('session.secret'), (error, user) => {
                if (!error) {
                    res.user = user;
                    return next();
                }

                return redirect();
            });
        }

        return redirect();
    },

    api: (req, res, next) => {
        let token = (req.body && req.body['access_token']) || (req.query && req.query['access_token']) || req.headers['x-access-token'];

        if (token) {
            return jwt.verify(token, config.get('session.secret'), (error, user) => {
                if (!error) {
                    res.user = user;
                    return next();
                }

                return next({
                    status: 401,
                    messsage: 'Not authorized'
                });
            });
        }

        return next({
            status: 401,
            messsage: 'Not authorized'
        });
    }
};
