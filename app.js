const config = require('config');
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const winston = require('winston');
const expressWinston = require('express-winston');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const flash = require('express-flash');

const sequelize = require('./lib/database');
const models = require('./models')(sequelize);

const routes = require('./routes');

const app = express();

app.set('models', models);
app.set('trust proxy', 1);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.locals.self = require('./lib/template')();

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(expressWinston.logger({
    transports: [
        new winston.transports.Console({
            json: false,
            colorize: true
        })
    ],
    meta: false,
    msg: "HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}",
    expressFormat: true,
    colorize: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(config.get('assets.baseUrl'), express.static(path.join(__dirname, 'public')));
app.use(cookieParser(config.get('session.secret')));
app.use(session({
    secret: config.get('session.secret'),
    resave: config.get('session.resave'),
    saveUninitialized: config.get('session.saveUninitialized'),
    cookie: {
        secure: config.get('session.cookie.secure'),
        maxAge: config.get('session.cookie.maxAge')
    }
}));
app.use(flash());

app.use(routes());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    if (req.xhr || req.originalUrl.match(/^\/api/) !== null) {
        return res.json({
            error: err.message || err
        });
    }

    res.render('error');
});

module.exports = app;
