const crypto = require('crypto');
const config = require('config');

const hashPassword = (password) => {
    let hmac = crypto.createHmac(
        String(config.get('password.algorithm')),
        String(config.get('password.salt'))
    );
    hmac.update(password);

    return hmac.digest('hex');
};

/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    const Admin = sequelize.define("admin", {
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            set(password) {
                return hashPassword(password);
            }
        }
    });

    Admin.prototype.validPassword = (admin, password) => {
        return admin.password === hashPassword(password);
    };

    Admin.hashPassword = hashPassword;

    return Admin;
};
