/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    return sequelize.define("tag", {
        name: {
            type: DataTypes.STRING
        }
    })
};