const path = require('path');
/**
 * @param {Sequelize} sequelize
 */
module.exports = (sequelize) => {
    // Import models
    const models = {
        Author: sequelize.import(path.resolve(__dirname, 'author.js')),
        Channel: sequelize.import(path.resolve(__dirname, 'channel.js')),
        Category: sequelize.import(path.resolve(__dirname, 'category.js')),
        Video: sequelize.import(path.resolve(__dirname, 'video.js')),
        Tag: sequelize.import(path.resolve(__dirname, 'tag.js')),
        Admin: sequelize.import(path.resolve(__dirname, 'admin.js')),
    };

    // Associations

    // Video has one Category (belongsTo)
    models.Video.belongsTo(models.Category, {
        onDelete: 'cascade',
        foreignKey: {
            field: 'categoryId',
            allowNull: false,
        }
    });
    // Video has one Channel (belongsTo)
    models.Video.belongsTo(models.Channel, {
        onDelete: 'cascade',
        foreignKey: {
            field: 'channelId',
            allowNull: false,
        }
    });
    // Channel has one Author (belongsTo)
    models.Channel.belongsTo(models.Author, {
        onDelete: 'cascade',
        foreignKey: {
            field: 'authorId',
            allowNull: false,
        }
    });
    // Author has many channels (oneToMany or hasMany)
    models.Author.hasMany(models.Channel, {
        as: 'channels'
    });
    // Category has many videos (oneToMany or hasMany)
    models.Category.hasMany(models.Video, {
        as: 'videos'
    });
    // Channel has many videos (oneToMany or hasMany)
    models.Channel.hasMany(models.Video, {
        as: 'videos'
    });
    // Video has many comments (belongsToMany or manyToMany)
    models.Video.belongsToMany(models.Tag, {
        as: 'tags',
        through: 'videoTags'
    });

    return models;
};