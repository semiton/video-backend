/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    return sequelize.define("video", {
        status: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'active'
        },
        provider: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'youtube'
        },
        data: {
            type: DataTypes.JSON,
            allowNull: false,
            defaultValue: 'youtube'
        },
        title: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.TEXT
        }
    });
};