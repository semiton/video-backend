/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    return sequelize.define("author", {
        name: {
            type: DataTypes.STRING
        }
    })
};