/**
 * @param {Sequelize} sequelize
 * @param {Sequelize.DataTypes} DataTypes
 */
module.exports = (sequelize, DataTypes) => {
    return sequelize.define("channel", {
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        status: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'active'
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: true,
        }
    });
};