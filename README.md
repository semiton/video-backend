**Install**:

```
npm install
npm install bower -g
bower install
```
copy config/config/default.yml.template - config/config/default.yml

add admin in DB:
```
INSERT INTO `admins` VALUES ('1', 'admin', 'e1a25b556668bc86e45c6d358b3f5100029e749a5877281a132018389688e418', '2018-02-25 00:46:10', '2018-02-25 00:46:12');
```


**Run:**
```
npm start
```

Routes:
Admin - /admin
API - /api

API auth:
get /api/auth/token with params _username_ & _password_, copy response token and send to api with
header X-Access-Token (or GET-param access_token)

Current API routes:
```
/api/authors/
/api/authors/:id
/api/channels/(optional GET:authorId)
/api/channels/:id
/api/videos/ (optional GET:channelId,GET:authorId)
/api/videos/:id
```

